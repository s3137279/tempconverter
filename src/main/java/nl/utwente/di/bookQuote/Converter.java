package nl.utwente.di.bookQuote;

public class Converter {
    public String convert(String value) {
        if (value == null) {
            return "Input value is null";
        }

        // Convert the input string to a double
        double celsius = Double.parseDouble(value);

        // Convert Celsius to Fahrenheit
        double fahrenheit = (celsius * 9 / 5) + 32;

        // Return the result as a string
        return String.format("%.2f", fahrenheit);
    }

}
