package nl.utwente.di.bookQuote;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Celsius extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Converter converter;

    public void init() throws ServletException {
        converter = new Converter();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Conversion from Celsius to Fahrenheit";

        // Done with string concatenation only for the demo
        // Not expected to be done like this in the project
        out.println("<!DOCTYPE HTML>\n" + "<html lang=\"en\">\n" + "<head>\n" + "<title>" + title +
                            "</title>\n" + "<link rel=\"stylesheet\" href=\"styles.css\">\n" +
                            "<meta charset=\"UTF-8\">\n" + "</head>\n" + "<body>\n" + "<h1>" + title + "</h1>\n" +
                            "<p>Celsius: " + request.getParameter("degrees") + " degrees"+ "</p>\n" +
                            "<p>Fahrenheit: " + converter.convert(request.getParameter("degrees"))+ " degrees" + "</p>\n" +
                            "</body>\n" + "</html>");

    }
}
