package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {

    private Map<String, Double> bookPrices;

    public Quoter() {
        bookPrices = new HashMap<>();
        bookPrices.put("1", 10.0);
        bookPrices.put("2", 45.0);
        bookPrices.put("3", 20.0);
        bookPrices.put("4", 35.0);
        bookPrices.put("5", 50.0);
        // Default price for other ISBNs
        bookPrices.put("default", 0.0);
    }

    public double getBookPrice(String isbn) {
        if (bookPrices.containsKey(isbn)) {
            return bookPrices.get(isbn);
        } else {
            // If ISBN is not found, return the default price
            return bookPrices.get("default");
        }
    }
}

